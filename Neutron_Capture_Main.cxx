#include "ConfigSvc.h"
#include "AmLi_Neutron_Capture.h"
#include "Cf252_Neutron_Capture.h"
#include <string>

int main(int argc, char *argv[]) {
  // Crate config svc to read from unique config file
  string alpacaTopDir = std::getenv("ALPACA_TOPDIR");
  string analysisName("Neutron_Capture");
  string configFile("config/Neutron_Capture.config");
  string configFileFullPath =
      alpacaTopDir + "/modules/" + analysisName + "/" + configFile;

  ConfigSvc *config = ConfigSvc::Instance(argc, argv, analysisName,
                                          alpacaTopDir, configFileFullPath);

  // Create analysis code and run
  Analysis *ana;
  if (config->configFileVarMap["whichAna"] == 0) {
    ana = new AmLi_Neutron_Capture();
    ana->Run(config->FileList, config->OutName, config->FileListTruth);
  }
  else if (config->configFileVarMap["whichAna"] == 1) {
    ana = new Cf252_Neutron_Capture();
    ana->Run(config->FileList, config->OutName, config->FileListTruth);
  }

  // Clean up
  delete ana;
  delete config;

  return 0;
}
