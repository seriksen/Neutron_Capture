#ifndef Cf252_Neutron_Capture_H
#define Cf252_Neutron_Capture_H

#include "Analysis.h"
#include "EventBase.h"
#include "Selector.h"
#include "Histograms.h"

#include <TTreeReader.h>
#include <string>

class SkimSvc;

class Cf252_Neutron_Capture : public Analysis {

public:
  /// Constructor
  Cf252_Neutron_Capture();
  /// Destructor
  ~Cf252_Neutron_Capture();
  /// Called once before the event loop
  void Initialize();
  /// Called once per event
  void Execute();
  /// Called once after event loop
  void Finalize();

  void Process_Event(double t0, std::string base_path);

protected:

  /// Give access to parameters defined in the Applier.config
  ConfigSvc *m_conf;
  /// Give access to Selector
  Selector *selector;
  /// Give access to Histogram fillers
  Histograms *histograms;

  std::vector<int> od_pulses_all;
  std::vector<int> od_pulses_significant;
};

#endif
