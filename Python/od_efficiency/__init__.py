__author__ = """Sam Eriksen"""
__email__ = """eriksesr@gmail.com"""

from . import background_baccarat_root
from . import reduced_analysis
from .version import __version__
