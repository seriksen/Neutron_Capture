#include "AmLi_Neutron_Capture.h"

#include "Analysis.h"
#include "ConfigSvc.h"
#include "CutsBase.h"
#include "EventBase.h"
#include "HistSvc.h"
#include "Logger.h"
#include "SkimSvc.h"
#include "SparseSvc.h"

AmLi_Neutron_Capture::AmLi_Neutron_Capture() : Analysis() {

  // Setup config service
  m_conf = ConfigSvc::Instance();

  // Load ROOT branches
  m_event->IncludeBranch("pulsesTPC");
  m_event->IncludeBranch("eventTPC");
  m_event->IncludeBranch("pulsesSkin");
  m_event->IncludeBranch("pulsesODHG");
  m_event->IncludeBranch("ss");

  m_event->Initialize();
  m_cuts->sr1()->Initialize();

  selector = new Selector(m_event, m_conf);
  histograms = new Histograms(m_event, m_conf, m_hists);

}

AmLi_Neutron_Capture::~AmLi_Neutron_Capture() {
}

void AmLi_Neutron_Capture::Initialize() { INFO("Initializing AmLi_Neutron_Capture Analysis"); }

void AmLi_Neutron_Capture::Execute() {

  int trigger{selector->Check_Trigger()};
  std::string base_path{std::to_string(trigger)};
  base_path = "All_Triggers";

  //if (trigger != 1 || trigger != 128) return;

  // Get OD Pulses
  od_pulses_all = selector->OD_Get_Pulse_IDs();
  od_pulses_noise = selector->OD_Get_Pulse_IDs_Noise_Cut(od_pulses_all);
  od_pulses_large = selector->OD_Get_Pulse_IDs_Large(od_pulses_noise);

  histograms->Histogram_OD_Pulses(base_path + "/OD_Pulses",
                                  od_pulses_noise);
  histograms->Histogram_OD_Pulses(base_path + "/OD_Pulses_Large",
                                  od_pulses_noise);

  // if single scatter
  if ((*m_event->m_singleScatter)->nSingleScatters > 0) {
    base_path += "/SS";
    double tpc_time{
        (*m_event->m_tpcPulses)
            ->pulseStartTime_ns[(*m_event->m_singleScatter)->s1PulseID]};
    histograms->Neutron_Capture(base_path, tpc_time, od_pulses_large, false);

    if (Check_SR1_Cuts()) {
      base_path += "/SR1_Cuts";
      histograms->Neutron_Capture(base_path, tpc_time, od_pulses_large, false);

      float s1Area{(*m_event->m_singleScatter)->correctedS1Area_phd};
      float s2Area{(*m_event->m_singleScatter)->correctedS2Area_phd};

      float sigma{m_event->CalculateNRBandDistance(s1Area, log10(s2Area))};
      if (sigma >= -1 && sigma <= 1) {
        base_path += "/NR_Band";
        histograms->Neutron_Capture(base_path, tpc_time, od_pulses_large, false);
      }
    }
  }
  return;
}

void AmLi_Neutron_Capture::Finalize() {
  INFO("Finalizing AmLi_Neutron_Capture Analysis");
}

bool AmLi_Neutron_Capture::Check_SR1_Cuts() {
  int s2_id{(*m_event->m_singleScatter)->s2PulseID};
  int s1_id{(*m_event->m_singleScatter)->s1PulseID};
  if (m_cuts->sr1()->TPCMuonVeto() &&
      m_cuts->sr1()->PassesETrainVeto(s2_id, "S2")) {
    if (m_cuts->sr1()->IsValidXY(s2_id) && m_cuts->sr1()->S2Width_SS()) {
      if (m_cuts->sr1()->S1TBA_SS() && m_cuts->sr1()->PassHSCCut(s1_id) &&
          m_cuts->sr1()->Stinger(s1_id)) {
        if (m_cuts->sr1()->Fiducial()) {
          return true;
        }
      }
    }
  }
  return false;
}
