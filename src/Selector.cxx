#include "Selector.h"
#include "Analysis.h"
#include "ConfigSvc.h"
#include "Logger.h"
#include "ConfigSvc.h"
#include "EventBase.h"
#include "Logger.h"
#include <TTreeReader.h>
#include <TMath.h>

Selector::Selector(EventBase *eventBase, ConfigSvc *config) {
  m_event = eventBase;
  m_conf = config;

  INFO("Selector Setup");

}

Selector::~Selector() {}


int Selector::Check_Trigger() {
  return (*m_event->m_eventHeader)->triggerType;
}

bool Selector::OD_Noise_Cut(int pulse_id) {
  if ((*m_event->m_odHGPulses)->coincidence[pulse_id] > 5
      && (*m_event->m_odHGPulses)->pulseArea_phd[pulse_id] > 4.5
      && (*m_event->m_odHGPulses)->peakAmp[pulse_id] / (*m_event->m_odHGPulses)->pulseArea_phd[pulse_id] > 5e-3)
    return true;
  return false;
}


bool Selector::OD_Large_Cut(int pulse_id) {
  if ((*m_event->m_odHGPulses)->coincidence[pulse_id] > 30
      && (*m_event->m_odHGPulses)->pulseArea_phd[pulse_id] > 50.)
    return true;
  return false;
}


std::vector<int>
Selector::OD_Get_Pulse_IDs() {
  std::vector<int> pulse_ids;
  for (int i = 0; i < (*m_event->m_odHGPulses)->nPulses; ++i) {
    pulse_ids.push_back(i);
  }
  return pulse_ids;
}

std::vector<int>
Selector::OD_Get_Pulse_IDs_Noise_Cut(std::vector<int> pulse_ids) {
  std::vector<int> pulse_ids_selected;
  int coincidence;
  float pulseArea_phd;
  for (auto &&id : pulse_ids) {
    if (OD_Noise_Cut(id)) pulse_ids_selected.push_back(id);
  }
  return pulse_ids_selected;
}

std::vector<int>
Selector::OD_Get_Pulse_IDs_Large(std::vector<int> pulse_ids) {
  std::vector<int> pulse_ids_selected;
  int coincidence;
  float pulseArea_phd;
  for (auto &&id : pulse_ids) {
    if (OD_Large_Cut(id)) pulse_ids_selected.push_back(id);
  }
  return pulse_ids_selected;
}

int Selector::Is_Skin() {

  float largest_phd{-1};
  int largest_coincidence{-1};
  int largest{-1};
  float phd;
  int coincidence;
  for (int i=0; i<(*m_event->m_skinPulses)->nPulses; ++i) {

    phd = (*m_event->m_skinPulses)->pulseArea_phd[i];
    coincidence = (*m_event->m_skinPulses)->coincidence[i];

    if (phd > largest_phd && coincidence > largest_coincidence) {
      largest_phd = phd;
      largest = i;
      largest_coincidence = coincidence;
    }
  }
  return largest;
}

bool Selector::Is_SS() {
  return (*m_event->m_singleScatter)->nSingleScatters > 0;
}

bool Selector::TPC_Pulse_Shape_Cuts() {

  bool valid{true};
  int s1ID{(*m_event->m_singleScatter)->s1PulseID};
  int s2ID{(*m_event->m_singleScatter)->s2PulseID};

  double s1Width = (*m_event->m_tpcPulses)->rmsWidth_ns[s1ID];
  double s2Width = ((*m_event->m_tpcPulses)->areaFractionTime95_ns[s2ID] - (*m_event->m_tpcPulses)->areaFractionTime5_ns[s2ID]) / 1000.;
  double drift = (*m_event->m_singleScatter)->driftTime_ns / 1000.;
  double s2AftRatio = ((float)(*m_event->m_tpcPulses)->areaFractionTime5_ns[s2ID] / (float)(*m_event->m_tpcPulses)->areaFractionTime50_ns[s2ID]);

  if ( (s2AftRatio < 0.8) && (s2AftRatio > 0.4) && (s1Width < 85.) ) {
    if (s2Width < 0.2 + 0.045 * sqrt(drift)) {
      return false;
    }
    return true;
  }
  return false;
}

bool Selector::NR_Band() {

  if ((*m_event->m_singleScatter)->correctedS1Area_phd > g_nr_s1.back()) return false;

  int bin;
  auto upper =
      std::upper_bound(g_nr_s1.begin(),
                       g_nr_s1.end(),
                       (*m_event->m_singleScatter)->correctedS1Area_phd);
  bin = (std::distance(g_nr_s1.begin(),
                       upper) - 1);
  if (log10((*m_event->m_singleScatter)->correctedS2Area_phd) > g_nr_s2_min[bin] ) {
    if (log10((*m_event->m_singleScatter)->correctedS2Area_phd) < g_nr_s2_max[bin] )
      return true;
  }
  return false;
}

bool Selector::FID() {

  float idealFiducialLowerBoundary_us = 945.;
  float idealFiducialUpperBoundary_us = 67.;
  float standoffDistance_cm = 4;
  const std::array<double, 7> idealFiducialWallFit = {
      72.4403, 0.00933984, 5.06325e-5, 1.65361e-7, 2.92605e-10, 2.53539e-13, 8.30075e-17
  };

  bool result{false};

  float drift_us = (*m_event->m_singleScatter)->driftTime_ns / 1000.;

  // Z cut
  if ((drift_us < idealFiducialLowerBoundary_us) && (drift_us > idealFiducialUpperBoundary_us)) {

    double boundaryR = 0;
    for ( size_t i=0; i<idealFiducialWallFit.size(); ++i ) {
      boundaryR += idealFiducialWallFit.at(i)*TMath::Power(-drift_us, (double)i);
    }
    boundaryR -= standoffDistance_cm;

    // R cut
    double r = sqrt((*m_event->m_singleScatter)->correctedX_cm * (*m_event->m_singleScatter)->correctedX_cm
                    + (*m_event->m_singleScatter)->correctedY_cm * (*m_event->m_singleScatter)->correctedY_cm);
    if (r < boundaryR) {
      result = true;
    }
  }

  return result;

}