#include "Cf252_Neutron_Capture.h"

#include "Analysis.h"
#include "ConfigSvc.h"
#include "CutsBase.h"
#include "EventBase.h"
#include "HistSvc.h"
#include "Logger.h"
#include "SkimSvc.h"
#include "SparseSvc.h"

Cf252_Neutron_Capture::Cf252_Neutron_Capture() : Analysis() {

  // Setup config service
  m_conf = ConfigSvc::Instance();

  // Load ROOT branches
  m_event->IncludeBranch("pulsesTPC");
  m_event->IncludeBranch("eventTPC");
  m_event->IncludeBranch("pulsesSkin");
  m_event->IncludeBranch("pulsesODHG");
  m_event->IncludeBranch("ss");

  m_event->Initialize();
  m_cuts->sr1()->Initialize();

  selector = new Selector(m_event, m_conf);
  histograms = new Histograms(m_event, m_conf, m_hists);

}

Cf252_Neutron_Capture::~Cf252_Neutron_Capture() {
}

void Cf252_Neutron_Capture::Initialize() { INFO("Initializing Cf252_Neutron_Capture Analysis"); }

void Cf252_Neutron_Capture::Execute() {

  int trigger{selector->Check_Trigger()};
  std::string base_path{std::to_string(trigger)};
  base_path = "All_Triggers";

  int skin_event{selector->Is_Skin()};
  bool tpc_event{selector->Is_SS()};

  // Get OD Pulses
  od_pulses_all = selector->OD_Get_Pulse_IDs();
  od_pulses_significant = selector->OD_Get_Pulse_IDs_Large(od_pulses_all);

  histograms->Histogram_OD_Pulses(base_path + "/OD_Pulses",
                                  od_pulses_significant);

  // if single scatter
  double t0;
  if (tpc_event) {
    t0 = (*m_event->m_tpcPulses)
             ->pulseStartTime_ns[(*m_event->m_singleScatter)->s1PulseID];
    Process_Event(t0, base_path);
  }
  else if (skin_event) {
    t0 = (*m_event->m_skinPulses)->pulseStartTime_ns[skin_event];
    Process_Event(t0, base_path);
  }
  return;
}

void Cf252_Neutron_Capture::Finalize() {
  INFO("Finalizing Cf252_Neutron_Capture Analysis");
}

void Cf252_Neutron_Capture::Process_Event(double t0, std::string base_path) {

  int pre_t0_id{-1};
  float pre_t0_phd{-1.};
  int coinc_t0_id{-1};
  float coinc_t0_phd{-1.};
  int post_t0_id{-1};
  float post_t0_phd{-1.};
  std::string hist_base;
  double time_diff;

  for (auto &&id : od_pulses_significant) {

    time_diff = (*m_event->m_odHGPulses)->pulseStartTime_ns[id] - t0;

    if (time_diff < 400) {
      hist_base = base_path + "/Pre_400ns";
      if ((*m_event->m_odHGPulses)->pulseArea_phd[id] > pre_t0_phd) {
        pre_t0_id = id;
      }
    }
    else if (time_diff > 400) {
      hist_base = base_path + "/Post_400ns";
      if ((*m_event->m_odHGPulses)->pulseArea_phd[id] > post_t0_phd) {
        post_t0_id = id;
      }
      m_hists->BookFillHist(
          hist_base + "/TimeDifference_us",
          2000, 0., 2000.,
          time_diff / 1000);
    }
    else {
      hist_base = base_path + "/Coincidence_400ns";
      if ((*m_event->m_odHGPulses)->pulseArea_phd[id] > coinc_t0_phd) {
        coinc_t0_id = id;
      }
    }
    m_hists->BookFillHist(hist_base + "/pulseArea_phd",
                          4000., 0., 4000.,
                          (*m_event->m_odHGPulses)->pulseArea_phd[id]);
    m_hists->BookFillHist(hist_base + "/coincidence",
                          120., 0., 120.,
                          (*m_event->m_odHGPulses)->coincidence[id]);

    m_hists->BookFillHist(
        base_path + "/TimeDifference_us",
        2000, -2000., 2000.,
        time_diff / 1000);

    //if (process_position) {
    //  OD_Positions(hist_base, id);
    //}
  }

  if (post_t0_id > -1) {
    m_hists->BookFillHist(base_path + "/Post_400ns/Largest/pulseArea_phd",
                          4000., 0., 4000.,
                          (*m_event->m_odHGPulses)->pulseArea_phd[post_t0_id]);
    m_hists->BookFillHist(base_path + "/Post_400ns/Largest/coincidence",
                          120., 0., 120.,
                          (*m_event->m_odHGPulses)->coincidence[post_t0_id]);
    time_diff = (*m_event->m_odHGPulses)->pulseStartTime_ns[post_t0_id] - t0;
    m_hists->BookFillHist(
        base_path + "/Post_400ns/Largest/TimeDifference_us",
        2000, 0., 2000.,
        time_diff / 1000);
    m_hists->BookFillHist(base_path + "/Post_400ns/Largest/phd_vs_time",
                          80, 0, 2000,
                          200, 0., 2000.,
                          (*m_event->m_odHGPulses)->pulseArea_phd[post_t0_id], time_diff);
    histograms->OD_Positions(base_path + "/Post_400ns/Largest", post_t0_id);
  }
}